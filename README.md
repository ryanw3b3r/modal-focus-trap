# Modal Focus Trap

VueJS 3 CompositionAPI component in TypeScript.

This component will trap Tab key, Shift + Tab, ArrowDown and ArrowUp keys
in modals or other elements that it will wrap around.

## How to use?

Just add it to your project just like any of yours ordinary
components. It's not installed via npm or anything - full flexibility.

When you copy code, use it like this:

```js
import ModalFocusTrap from '../path/to/this/ModalFocusTrap.vue'
```

```html
<ModalFocusTrap> <!-- this will trap curson inside div below //-->
  <div>
    <input type="text" v-model="something">
    <input type="password" v-model="else">
  </div>
</ModalFocusTrap>
```

## Author

Ryan Weber, One Smart Space Ltd. © 2023 All rights reserved.
